'use strict'
// global escope
// this.state = 0
const coin = {
    state: 0,
    // method
    flip: function() {
        // 1. Um ponto: Randomicamente configura a propriedade “estado” do 
        // seu objeto moeda para ser um dos seguintes valores:
        // 0 ou 1: use "this.state" para acessar a propriedade "state" neste objeto.
        console.log(coin.state) //0
        // declara variavel
        // fazer jogadas
        let aleatorio = Math.floor(Math.random() * 2)
        this.state = aleatorio
        console.log(this.state)
        
        // local escope
    },
    
    toString: function() {
        // 2. Um ponto: Retorna a string "Heads" ou "Tails", dependendo de como
        //  "this.state" está como 0 ou 1.
        // condição para retornar a string 'Heads'
        if(this.state == 0) {
            console.log('Heads')
            return 'Heads'
        }
        // condição para retornar a string 'Tails'
        if(this.state == 1 ) {
            console.log('Tails')
            return "Tails"
        }
        
    },
    toHTML: function() {
        const image = document.createElement('img');
        // 3. Um ponto: Configura as propriedades do elemento imagem 
        // para mostrar a face voltada para cima ou para baixo dependendo
        // do valor de this.state está 0 ou 1.
        if(this.state == 0) {
        image.src = 'image/heroi1.jpg';
        }
        if(this.state == 1) {
            image.src = 'image/heroi2.jpg';
        
         }
         document.body.appendChild(image)
        

    }
 };
 
 function display20Flips() {
    const results = [];
    // 4. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma string na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
    for( let i = 0; i< 20;i++) {
        coin.flip()
         let string = document.createElement('span');
        string.innerHTML = coin.toString()
        // puxa já altera a variável
        results.push(coin.toString())
        document.body.appendChild(string)
        

    }
    return results

 
 }
 
 function display20Images() {
    const results = [];
    // 5. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma imagem na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
    for( let i = 0; i< 20;i++) {
        coin.flip()
        results.push(coin.toString())
        coin.toHTML()
    }
    return results
//  criar variavel, utilizar innerHTML, 
 }
//  chamar a funhção do objeto
 coin.flip()
 coin.toString()
coin.toHTML()
// não fazem parte do const coin {}
display20Flips()
display20Images()
